import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Slides from './slides/Slides';
import { Router, Route, browserHistory } from 'react-router';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/(:slideNumber)" component={Slides} />
  </Router>,
  document.getElementById('app')
);
