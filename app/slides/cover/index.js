import './cover.scss';
import * as React from 'react';
import { Motion, spring } from 'react-motion';

export default class Cover extends React.Component {
  render() {
    return (
      <div>
          <div className="cover">
            <h1>Webpack</h1>
            <Motion defaultStyle={{x: 0}} style={{x: spring(15, {damping: 0, stiffness: 1})}}>
            {
              value => {
                let style = {
                  transformStyle: 'preserve-3d',
                  transform: `translateY(${value.x}px)`
                };

                return (
                  <div style={style}><img src="static/webpack.svg" /></div>
                );
              }
            }
            </Motion>
            <div className="cover-shadow" />
        </div>
      </div>
    );
  }
}