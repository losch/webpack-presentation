import React from 'react';
const PropTypes = React.PropTypes;

const LEFT_ARROW_KEY = '37';
const RIGHT_ARROW_KEY = '39';

class Button extends React.Component {
  render() {
    const { onClick, isDisabled, children } = this.props;
    return (
      <button className="mui-btn mui-btn--primary mui-btn--flat"
              disabled={isDisabled}
              onClick={() => onClick()}>{children}</button>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool,
  children: PropTypes.element.isRequired
};

export default class SlideControls extends React.Component {
  checkKey(e) {
    e = e || window.event;

    if (e.keyCode == LEFT_ARROW_KEY) {
      if (this.props.slideNumber > 0) {
        this.props.onPrevious();
      }
    }
    else if (e.keyCode == RIGHT_ARROW_KEY) {
      if (this.props.slideNumber + 1 < this.props.totalSlides) {
        this.props.onNext();
      }
    }
  }

  componentDidMount() {
    document.onkeydown = (e) => this.checkKey(e);
  }

  componentWillUnmount() {
    document.onkeydown = null;
  }

  render() {
    const { onNext, onPrevious, slideNumber, totalSlides } = this.props;
    const slideNumberText = '' + (slideNumber + 1) + ' / ' + totalSlides;
    const isPreviousDisabled = slideNumber <= 0;
    const isNextDisabled = slideNumber >= totalSlides - 1;
    
    return (
      <div className="slide-controls">
        <div className="slide-controls-panel mui-panel">
          <Button onClick={() => onPrevious()}
                  isDisabled={isPreviousDisabled}>&lt;</Button>
          <span className="slideNumberText">{slideNumberText}</span>
          <Button onClick={() => onNext()}
                  isDisabled={isNextDisabled}>&gt;</Button>
        </div>
      </div>
    );
  }
}
  
SlideControls.propTypes = {
  onNext: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
  slideNumber: PropTypes.number.isRequired,
  totalSlides: PropTypes.number.isRequired
};
