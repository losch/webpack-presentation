Webpack - Mikä on Webpack?
==========================

https://webpack.github.io

* JavaScript-bundleri
* Lukee moduuleja ja päättelee moduuleiden väliset riippuvuudet ja tuottaa
  bundleja. Esim. yksi iso JavaScript-tiedosto yhden sivun sovellukselle tai
  monta pienempää JavaScript-tiedostoa monen sivun sovellukselle.
* Loadereilla voidaan lisätä tukea eri tiedostoformaateille.
  Esim. modernit JavaScript-versiot, TypeScript, CSS, SASS, kuvatiedostot jne.
* Plugineilla voidaan muuttaa miten Webpack toimii ja niitä yleensä käytetään
  määrittelyssä miten bundle luodaan. Esim. minifiointi, bundlejen yhteisen
  koodin jakaminen tiettyyn bundleen.
* Hienot kehitystyökalut: Webpack dev-server, source maps,
  hot module replacement
