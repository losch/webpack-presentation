import './summary.scss';
import * as React from 'react';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export default class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChuck: false,
      isExploding: false
    };
  }

  toggleChuck() {
    let willExplode = !this.state.isChuck;

    if (willExplode) {
      setTimeout(() => this.setState({isExploding: false}), 3000);
    }

    this.setState({
      isChuck: !this.state.isChuck,
      isExploding: willExplode
    });
  }

  renderGiovanni() {
    return (
      <div className="summary summary-giovanni"
           onClick={() => this.toggleChuck()}>
        <img className="summary-image" src="/static/frontenddev.jpg" />
        <div className="summary-caption">
          “Front end programmer”<br/>Giovanni Battista Moroni, 1570–1575
        </div>
      </div>
    );
  }

  renderChuck(isAppearing) {
    let className = 'summary' + (isAppearing ? ' is-appearing' : '');
    
    return (
      <div key="chuck" className={className} onClick={() => this.toggleChuck()}>
        <img className="summary-image" src="/static/chuck.jpg" />
        <div className="summary-caption">
          “Front end programmer with Webpack”
        </div>
      </div>
    );
  }

  renderExplosions() {
    let explosions = [];
    for (let i = 0; i < 100; i++) {
      let style = {
        position: 'absolute',
        top: getRandomInt(0, 90) + '%',
        left: getRandomInt(0, 90) + '%',
        opacity: getRandomInt(10, 100) / 100.0,
        zIndex: 1000
      };
      explosions.push(style);
    }

    return (
      <div style={{position: 'fixed', left: 0, top: 0, bottom: 0, right: 0}}>
        {
          explosions.map((explosionStyle) =>
            <img style={explosionStyle}
                 className="summary-image"
                 src="/static/explosion.gif" />
          )
        }
        <img style={{top: '50%',
                     display: 'block',
                     position: 'absolute',
                     zIndex: 2000,
                     width: '128px',
                     left: '50%',
                     marginTop: '-100px',
                     marginLeft: '-100px'}}
             src="static/webpack.png" />
      </div>
    );
  }

  render() {
    if (this.state.isExploding) {
      return <div>{this.renderChuck(true)}{this.renderExplosions()}</div>;
    }
    else {
      return this.state.isChuck ?
        <div>{this.renderChuck(false)}</div> :
        this.renderGiovanni();
    }
  }
}
