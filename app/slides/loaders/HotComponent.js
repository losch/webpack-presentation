import './HotComponent.scss';
import React from 'react';
import Wow from './Wow';

export default class HotComponent extends React.Component {
  onClick() {
    alert('Hyvin tehty');
  }

  render() {
    return (
      <div className="example-component example-hot-component">
        <Wow />
        <h1>Hieno otsikko</h1>
        <button className="mui-btn" onClick={() => this.onClick()}>Paina tästä</button>
      </div>
    );
  }
}
