import * as React from 'react';
import MarkDownComponent from '../components/MarkDownComponent';
import HotComponent from './HotComponent';

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      step: 1
    };
  }

  setTimeout() {
    this._timer = setTimeout(() => this.increase(), 1000);
  }

  increase() {
    this.setState({
      count: this.state.count + this.state.step
    });
    this.setTimeout();
  }

  onStepChanged() {
    let value = parseInt(this.refs.input.value);
    if (!isNaN(value)) {
      this.setState({
        step: value
      });
    }
  }

  componentWillMount() {
    this.setTimeout();
  }

  componentWillUnmount() {
    if (this._timer) {
      clearTimeout(this._timer);
    }
  }

  render() {
    return (
      <div className="example-component">
        Laskuri: { this.state.count }
        <div>
          <label htmlFor="stepper">+</label>
          <input id="stepper"
                 ref="input"
                 onChange={() => this.onStepChanged()}
                 type="text"
                 value={this.state.step} />
        </div>
      </div>
    );
  }
}

export default class HotReload extends React.Component {
  render() {
    return (
      <div>
        <MarkDownComponent contents={require('raw!./2.md')} />
        <div className="slide-example mui-panel">
          <Counter />
          <HotComponent />
        </div>
      </div>
    );
  }
}