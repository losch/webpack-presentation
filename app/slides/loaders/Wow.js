import React from 'react';
import { Motion, spring } from 'react-motion';

export default class Wow extends React.Component {
  render() {
    const h1Style = {
      color: '#111155',
      textShadow: '2px 2px #FFEE11'
    };

    return (
      <Motion defaultStyle={{x: 0.8}} style={{x: spring(1.52, {damping: 0, stiffness: 2})}}>
        {
          value => {
            let style = {
              width: '100px',
              marginLeft: 'auto',
              marginRight: 'auto',
              transform: `scale(${value.x}, ${value.x})`
            };

            return <div style={style}><h1 style={h1Style}>Wow!</h1></div>;
          }
        }
      </Motion>
    );
  }
}
