import './slides.scss';

import * as React from 'react';
const PropTypes = React.PropTypes;
import { browserHistory } from 'react-router';

import SlideControls from './components/SlideControls';
import Cover from './cover';
import Summary from './summary';
import MarkDownComponent from './components/MarkDownComponent';
import HotReload from './loaders';

export default class Slides extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slideNumber: parseInt(props.params.slideNumber || '1') - 1
    };
  }

  static get SLIDES() {
    return [
      <Cover />,

      // What is Webpack?
      <MarkDownComponent contents={require('raw!./what-is-webpack/0.md')} />,
      <MarkDownComponent contents={require('raw!./what-is-webpack/1.md')} />,
      <MarkDownComponent contents={require('raw!./what-is-webpack/1-1.md')} />,
      <MarkDownComponent contents={require('raw!./what-is-webpack/2.md')} />,
      <MarkDownComponent contents={require('raw!./what-is-webpack/3.md')} />,
      <MarkDownComponent contents={require('raw!./what-is-webpack/4.md')} />,

      // Modules
      <MarkDownComponent contents={require('raw!./modules/0.md')} />,
      <MarkDownComponent contents={require('raw!./modules/1.md')} />,
      <MarkDownComponent contents={require('raw!./modules/2.md')} />,

      // Loaders
      <MarkDownComponent contents={require('raw!./loaders/0.md')} />,
      <MarkDownComponent contents={require('raw!./loaders/0-1.md')} />,
      <MarkDownComponent contents={require('raw!./loaders/1.md')} />,
      <HotReload />,
      <MarkDownComponent contents={require('raw!./loaders/3.md')} />,

      // Plugins
      <MarkDownComponent contents={require('raw!./plugins/1.md')} />,

      // Summary
      <Summary />
    ];
  }

  currentSlide() {
    return Slides.SLIDES[this.state.slideNumber];
  }

  nextSlide() {
    if (this.state.slideNumber <= Slides.SLIDES.length) {
      const nextSlide = this.state.slideNumber + 1;
      this.setState({slideNumber: nextSlide});
      browserHistory.push('/' + (nextSlide + 1));
    }
  }

  previousSlide() {
    if (this.state.slideNumber > 0) {
      const nextSlide = this.state.slideNumber - 1;
      this.setState({slideNumber: nextSlide});
      browserHistory.push('/' + (nextSlide + 1));
    }
  }

  render() {
    return (
      <div className="slide mui-container">
        { this.currentSlide() }
        <SlideControls onNext={() => this.nextSlide()}
                       onPrevious={() => this.previousSlide()}
                       slideNumber={this.state.slideNumber}
                       totalSlides={Slides.SLIDES.length} />
      </div>
    );
  }
}

Slides.propTypes = {
  params: PropTypes.shape({
    slideNumber: PropTypes.number
  })
};
