Presentation About Webpack JavaScript Bundling Tool (in Finnish)
================================================================

Requirements
------------

* [NodeJS and npm package manager](https://nodejs.org/en/download/)

Usage
-----

1. Install dependencies with ```npm install```
2. Start up development server ```npm run dev-server```
3. Open browser to ```http://localhost:9000```
4. Use left and right arrows or navigation component at bottom right corner
   to navigate between slides
5. Try hot loaders by modifying some slide contents, JS files or SCSS files
